import { Body, Controller, Get, Post } from '@nestjs/common';
import { patinhosRepository } from './repository/patinhos.repository';
import { PatinhosDto } from './DTO/patinhos-confirmacao.dto';

@Controller('letra')
export class LetraController {


    constructor(private readonly _repository : patinhosRepository){ }

    letra = {};

    @Get()
    async getMostrarLetra(){
      this.letra = await this._repository.mostrarLetra()
      return {
        letra: this.letra
      }
    }

    @Post()
    postReceberNumero(@Body() qtdPatinhos : PatinhosDto) {
      
        return this._repository.geradorDeLetra(qtdPatinhos.qtdPatinhos)
        
    }
    




}
