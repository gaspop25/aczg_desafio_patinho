import { IsInt, Max, Min } from "class-validator"

export class PatinhosDto {
  

    @IsInt({message: "A Quantidade deve ser um valor numérico inteiro"})
    @Min(1, {message: "A Quantidade deve ter um valor minímo de 1 , inteiro e positivo"})
    @Max(100, {message: "O valor máximo para a Quantidade é 100 patinhos"})
    qtdPatinhos: number



}
