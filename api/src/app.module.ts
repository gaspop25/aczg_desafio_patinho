import { Module } from '@nestjs/common';
import { LetraController } from './letra/letra.controller';
import { patinhosRepository } from './letra/repository/patinhos.repository';

@Module({
  imports: [],
  controllers: [ LetraController],
  providers: [patinhosRepository],
})
export class AppModule {}
