import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class HomeService {


  
  constructor(private http:HttpClient) { }

  private readonly URL = `https://3000-gaspop25-aczgdesafiopat-ehokjlwdjf5.ws-us100.gitpod.io`

   pegarLetra():Observable<any>{
    return this.http.get<any>(`${this.URL}/letra`)
  }


   PostNumeroDePatinhos(qtdPatinhos : number ){
    
     this.http.post<any>(`${this.URL}/letra`, {"qtdPatinhos": qtdPatinhos}).subscribe()
  }


}
