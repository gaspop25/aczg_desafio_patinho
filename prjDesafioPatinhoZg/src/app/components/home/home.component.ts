import { Component} from '@angular/core';
import { HomeService } from 'src/app/services/home.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { delay } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent  {

 

  constructor(private homeService : HomeService){
    this.mostraLetra()
  
  }

  qtdPatinhos : number = 5

 
 
 letra = {

  refrao : "",
  meio: "",
  fim : ""
 };

 

 value : number =0 


  enviarNumeroDePatinhos(qtdPatinhos : string ){
    this.value = parseFloat(qtdPatinhos);
    this.homeService.PostNumeroDePatinhos(this.value)
    console.log(this.value);
  }


  mostraLetra(){
    this.homeService.pegarLetra().subscribe(
      {
        next:(data: any)=>{
          console.log(data.letra)
          this.letra.refrao = data.letra.refrao
          this.letra.meio = data.letra.meio
          this.letra.fim = data.letra.final
          console.log(this.letra)
        },
        error:(erro)=>{
          console.error(erro)
        }
      }
    )
  }
}
